const fs = require("fs");
const path = require("path");
const loader = function (dir) {
  // initate reurn object
  const aret = {};

  // filters out subfolders
  const folders = fs
    .readdirSync(dir)
    .filter((res) => fs.lstatSync(path.resolve(dir, res)).isDirectory());

  // loops through subfolders and adds functions to object
  folders.forEach((folder) => {
    fs.readdirSync(path.resolve(dir, folder)).forEach(function (library) {
      const isLibrary =
          library.split(".").length > 0 && library.split(".")[1] === "js",
        libName = library.split(".")[0].toLowerCase();
      if (isLibrary) {
        aret[libName] = require(path.join(dir, folder, library));
      }
    });
  });
  // returns function object
  return aret;
};

module.exports.loader = loader;
